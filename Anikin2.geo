//+
SetFactory("OpenCASCADE");

//+
Circle(1) = {0, 0, 0, 5.66, 0, 2*Pi};
//+
Point(2) = {0.1, 1, 0, 0.01};
//+
Point(3) = {-0.1, 1, 0, 0.01};
//+
Point(4) = {-0.1, -1, 0, 0.01};
//+
Point(5) = {0.1, -1, 0, 0.01};
//+
Point(6) = {1, 0.1, 0, 0.01};
//+
Point(7) = {-1, 0.1, 0, 0.01};
//+
Point(8) = {-1, -0.1, 0, 0.01};
//+
Point(9) = {1, -0.1, 0, 0.01};
//+
Point(10) = {0.1, 0.15, 0, 0.01};
//+
Point(11) = {-0.1, 0.15, 0, 0.01};
//+
Point(12) = {-0.1, -0.15, 0, 0.01};
//+
Point(13) = {0.1, -0.15, 0, 0.01};
//+
Point(14) = {0.15, 0.1, 0, 0.01};
//+
Point(15) = {-0.15, 0.1, 0, 0.01};
//+
Point(16) = {-0.15, -0.1, 0, 0.01};
//+
Point(17) = {0.15, -0.1, 0, 0.01};
//+
Line(2) = {7, 15};
//+
Line(3) = {15, 11};
//+
Line(4) = {11, 3};
//+
Line(5) = {3, 2};
//+
Line(6) = {2, 10};
//+
Line(7) = {10, 14};
//+
Line(8) = {14, 6};
//+
Line(9) = {6, 9};
//+
Line(10) = {9, 17};
//+
Line(11) = {17, 13};
//+
Line(12) = {13, 5};
//+
Line(13) = {5, 4};
//+
Line(14) = {4, 12};
//+
Line(15) = {12, 16};
//+
Line(16) = {16, 8};
//+
Line(17) = {8, 7};
//+
Physical Curve("Outer", 300000) = {1};
//+
Physical Curve("Hot", 300200) = {8, 4, 16, 12};
//+
Curve Loop(1) = {16, 17, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
//+
Curve Loop(2) = {1};
//+
Curve Loop(3) = {4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 2, 3};
//+
Plane Surface(1) = {2, 3};
//+
Physical Curve("Cold", 300300) = {6, 2, 14, 10};
//+
Physical Curve("Trans X-Top", 300400) = {5};
//+
Physical Curve("Trans X-Bottom", 300500) = {13};
//+
Physical Curve("Trans Y-Left", 300600) = {17};
//+
Physical Curve("Trans Y-Right", 300700) = {9};
//+
Physical Curve("Trans -Slant Right Top", 300800) = {7};
//+
Physical Curve("Trans -Slant Right Bottom", 300900) = {11};
//+
Physical Curve("Trans -Slant Left Bottom", 301000) = {15};
//+
Physical Curve("Trans -Slant Left Top", 301100) = {3};
//+
Physical Surface("Inner", 301200) = {1};
//+
Show "*";
//+
Show "*";
